package nanar.nanar;


import nanar.TdNanarApplication;
import nanar.exceptions.ResourceNotFoundException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@SpringBootTest(classes = TdNanarApplication.class)
@AutoConfigureMockMvc
public class FilmServiceTest {

    @Mock
    private FilmRepository filmRepository;

    @InjectMocks
    private FilmJPAService filmJPAService;

    @Test
    public void getAllFilms_ReturnsListOfFilms() {
        // Arrange
        Film film = new Film(1L, "Film 1",2,12);
        Film film2 = new Film(2L, "Film 2",5,10);

        List<Film> films = new ArrayList<>();
        films.add(film);
        films.add(film2);


        when(filmRepository.findAll()).thenReturn(films);

        // Act
        List<Film> result = filmJPAService.getAllFilms();

        // Assert
        assertAll(
                ()->assertEquals(2, result.size()),
                ()->assertTrue(result.contains(film)),
                ()->assertTrue(result.contains(film2))
        );
    }

    @Test
    public void getFilmById_WithValidId_ReturnsFilm() {
        // Arrange
        Film film = new Film(1L, "Film 1",2,12);


        when(filmRepository.findById(film.getId())).thenReturn(Optional.of(film));

        // Act
        Film result = filmJPAService.getFilmById(film.getId());

        // Assert
        assertAll(
                //on verifie qu'on obtienne bien le bon
                ()->assertEquals(film, result),
                //
                ()->assertThrows(ResourceNotFoundException.class, ()->filmJPAService.getFilmById(175L))
        );

    }

    @Test
    public void whenCreatingFilm_shouldCreateFilm(){
        // Arrange
        Film film = new Film(1L, "Film 1",2,12);


        List<Film> films = new ArrayList<>();

        when(filmRepository.save(film)).thenAnswer(i -> {
            films.add(film);
            return film;
        });

        //Act
        filmJPAService.createFilm(film);

        //Assert
        assertTrue(films.contains(film));
    }




}
