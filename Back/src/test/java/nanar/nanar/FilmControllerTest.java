package nanar.nanar;


import nanar.TdNanarApplication;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@SpringBootTest(classes = TdNanarApplication.class)
@AutoConfigureMockMvc
public class FilmControllerTest {


    @Mock
    private FilmService filmService;

    @InjectMocks
    private FilmController filmController;

    @Test
    public void getAllFilms_ReturnsListOfFilms() {
        // Arrange
        Film film1 = new Film(1L, "Film 1",2,12);
        Film film2 = new Film(2L, "Film 2",5,10);
        List<Film> films = Arrays.asList(film1, film2);

        when(filmService.getAllFilms()).thenReturn(films);

        // Act
        List<Film> result = filmController.getAllFilms();

        // Assert
        assertEquals(2, result.size());
        assertTrue(result.contains(film1));
        assertTrue(result.contains(film2));
    }

    @Test
    public void getFilmById_WithValidId_ReturnsFilm() {
        // Arrange
        Long filmId = 1L;
        Film film = new Film(filmId,  "Film 1",2,12);

        when(filmService.getFilmById(filmId)).thenReturn(film);

        // Act
        Film result = filmController.getFilmById(filmId);

        // Assert
        assertEquals(film, result);
    }



}
