package nanar;


import nanar.nanar.Film;
import nanar.nanar.FilmRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TdNanarApplication implements CommandLineRunner {



    @Autowired
    private FilmRepository filmRepository;

    public static void main(String[] args) {
        SpringApplication.run(TdNanarApplication.class, args);
    }


    @Override
    public void run(String... args) throws Exception {


        Film film1 = new Film(1L, "Film 1",2,12);
        Film film2 = new Film(2L, "Film 2",5,10);

        filmRepository.save(film1);
        filmRepository.save(film2);

    }
}
