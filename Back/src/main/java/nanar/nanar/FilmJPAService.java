package nanar.nanar;

import nanar.exceptions.ResourceAlreadyExistsException;
import nanar.exceptions.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class FilmJPAService implements FilmService {

    @Autowired
    private FilmRepository filmRepository;
    
    @Override
    public List<Film> getAllFilms() {

        return filmRepository.findAll();
    }

    @Override
    public Film getFilmById(Long id) {
        Optional<Film> film = filmRepository.findById(id);
        if(film.isPresent()){
            return film.get();
        }
        else{
            throw new ResourceNotFoundException("Film",id);
        }
    }

  

    @Override
    public Film createFilm(Film newFilm) {
        if(filmRepository.existsById(newFilm.getId())){
            throw new ResourceAlreadyExistsException("Film",newFilm.getId());
        }
        return filmRepository.save(newFilm);
    }

    @Override
    public void updateFilm(Long id, Film newFilm) throws ResourceNotFoundException {
        if (!filmRepository.existsById(id)) { // Utilisez l'identifiant 'id' fourni comme paramètre
            throw new ResourceNotFoundException("Film", id);
        }
        newFilm.setId(id);
        filmRepository.save(newFilm);
    }

    
    @Override
    public void deleteFilm(Long id) throws ResourceNotFoundException {

        if(!filmRepository.existsById(id)){
            throw new ResourceNotFoundException("Film",id);
        }
        filmRepository.deleteById(id);
    
    }
}
