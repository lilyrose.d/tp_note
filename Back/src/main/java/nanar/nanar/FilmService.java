package nanar.nanar;

import nanar.exceptions.ResourceAlreadyExistsException;
import nanar.exceptions.ResourceNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public interface FilmService {
     List<Film> getAllFilms();

     void updateFilm(Long id, Film film) throws ResourceNotFoundException;

     Film getFilmById(Long id) ;
     Film createFilm(Film film) throws ResourceAlreadyExistsException;

     void deleteFilm(Long id) throws ResourceNotFoundException;



}
