package nanar.nanar;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import org.springframework.context.annotation.Primary;

import java.util.Objects;

@Entity
@Primary
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class Film {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idFilm;

    private String titre;

    private int niveau;

    private int note;

    public Film(Long idFilm, String titre, int niveau, int note) {
        this.idFilm = idFilm;
        this.titre = titre;
        this.niveau = niveau;
        this.note = note;
    }

    public Film() {

    }

    public Long getId() {
        return idFilm;
    }

    public void setId(Long idFilm) {
        this.idFilm = idFilm;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public int getNiveau() {
        return niveau;
    }

    public void setNiveau(int niveau) {
        this.niveau = niveau;
    }

    public int getNote() {
        return note;
    }

    public void setNote(int note) {
        this.note = note;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Film film = (Film) o;
        return niveau == film.niveau && note == film.note && Objects.equals(idFilm, film.idFilm) && Objects.equals(titre, film.titre);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idFilm, titre, niveau, note);
    }



}
