package nanar.nanar;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/films")
@CrossOrigin(origins = "*")
public class FilmController {
    
    private FilmService filmService;

    @Autowired
    public FilmController(FilmService filmService){
        this.filmService=filmService;
    }


    @GetMapping
    public List<Film> getAllFilms() {
        return filmService.getAllFilms();
    }

    @GetMapping("/id")
    public Film getFilmById(@PathVariable Long id) { return filmService.getFilmById(id);
    }


    @PostMapping
    public Film createFilm(@RequestBody Film film)  {
        return filmService.createFilm(film);
    }


    @PutMapping("/{id}")
    public ResponseEntity updateFilm(@PathVariable Long id, @RequestBody Film film) {
        filmService.updateFilm(id, film);
        return ResponseEntity.noContent().build();
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deleteFilm(@PathVariable Long id) {
        filmService.deleteFilm(id);
        return ResponseEntity.noContent().build();
    }

}
