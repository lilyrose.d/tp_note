import {NgModule} from '@angular/core';
import {RouterModule, Routes} from "@angular/router";
import {FilmListComponent} from "./Film/film-list/film-list.component";
import {FilmDetailsComponent} from "./Film/film-details/film-details.component";
import {FilmEditorComponent} from "./Film/film-editor/film-editor.component";


const routes: Routes = [
  { path: 'films', component: FilmListComponent },
  { path: 'films/edit', component: FilmEditorComponent },
  { path: 'films/:id', component: FilmDetailsComponent },
  ];
@NgModule({
  declarations: [],
  imports: [RouterModule.forRoot(routes, {bindToComponentInputs: true})],
  exports: [RouterModule]
})
export class AppRouter {}


