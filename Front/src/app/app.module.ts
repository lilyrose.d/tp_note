import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppComponent} from './app.component';
import {HttpClientModule} from "@angular/common/http";


import {FormsModule} from "@angular/forms";
import {FilmEditorComponent} from "./Film/film-editor/film-editor.component";
import {FilmDetailsComponent} from "./Film/film-details/film-details.component";
import {FilmListComponent} from "./Film/film-list/film-list.component";
import {AppRouter} from "./app-router";



@NgModule({
  declarations: [
    AppComponent,
    FilmListComponent,
    FilmDetailsComponent,
    FilmEditorComponent,

  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRouter,
    FormsModule,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
