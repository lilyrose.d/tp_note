import {Component} from '@angular/core';
import {FilmService} from "../film.service";
import { Film} from "../Film";
import {Router} from "@angular/router";

@Component({
  selector: 'app-film-list',
  templateUrl: './film-list.component.html',
  styleUrls: ['./film-list.component.css']
})
export class FilmListComponent {

  films: Film[];
  filmsOriginaux: Film[];
  creating: any;


  constructor(
    private filmService: FilmService,
    private router: Router
  ) {
    this.films = [];
    this.filmsOriginaux = [];


    filmService.get()
      .subscribe((films) => {
        this.films = films;
        this.filmsOriginaux = [...films]; // Copier les films pour les conserver comme originaux
      });
  }

  openFilmDetails(film: Film) {
    this.router.navigate(['/films/'+film.id],
      {state: {film: film, solo: true}})
  }

  openCreateFilm() {
    this.router.navigate(['/films/edit'],
      {state: {creating: true}})
  }



  reinitialiserFilms(): void {
    this.films = [...this.filmsOriginaux];
  }


}
