import {Injectable} from '@angular/core';
import {Film} from "./Film";
import {GenericService} from "../utils/generic.service";

@Injectable({
  providedIn: 'root'
})
export class FilmService extends GenericService<Film>{

  protected override url = "http://localhost:8080/films"
  protected override className = "Film"

}
