import {Component, Input} from '@angular/core';
import {FilmService} from "../film.service";
import {Film} from "../Film";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-film-details',
  templateUrl: './film-details.component.html',
  styleUrls: ['./film-details.component.css']
})
export class FilmDetailsComponent {

  @Input() film!: Film
  @Input() solo: boolean = true

  constructor(
      private filmService: FilmService,
      private router: Router,
      private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit(){
    if(this.film==null) {
      this.film=history.state.film
      console.log(history)
      console.log(history.state.film==null)
      if (history.state.film==null) {
        this.filmService.getById(Number(this.activatedRoute.snapshot.paramMap.get("id")))
            .subscribe(film=>this.film=film)
      }
    }
    this.solo=history.state.solo
  }

  delete(){
    this.filmService.delete(this.film)
        .subscribe(()=>this.router.navigate(['/films']))
  }

  update() {
    this.router.navigate(["/films/edit"],
        {state: {film: this.film,
                        updating: true}})
  }

}
