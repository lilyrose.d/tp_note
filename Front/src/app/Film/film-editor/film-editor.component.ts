import {Component, OnInit} from '@angular/core';
import {Film} from "../Film";
import {FilmService} from "../film.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-film-editor',
  templateUrl: './film-editor.component.html',
  styleUrls: ['./film-editor.component.css']
})
export class FilmEditorComponent implements OnInit{

  film: Film = {} as Film
  updating: boolean = false
  creating: boolean = false

  constructor(
      private filmService: FilmService,
      private router: Router
  ) {}

  ngOnInit() {
    if (history.state.film != null){
      this.film = history.state.film;
    }
    this.creating = history.state.creating;
    this.updating = history.state.updating;
  }

  edit() {
    console.log('Méthode edit() appelée.');
    if (this.updating) {
      this.filmService.update(this.film)
        .subscribe(() => this.processUpdate());
    } else if (this.creating) {
      this.filmService.create(this.film)
        .subscribe((location) => this.processCreate(location));
    }
  }

  processUpdate() {
    this.router.navigate(["/films/"+this.film.id],
      {state: {film: this.film}})
  }

  processCreate(url: string) {
    console.log(url)
    this.router.navigate([url])
  }
}
